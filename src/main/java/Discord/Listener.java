package Discord;

import sx.blah.discord.api.events.EventSubscriber;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;

public class Listener {
    @EventSubscriber
    public void onMessageEvent(MessageReceivedEvent event) {
        if(event.getMessage().getContent().length() >= 5 && event.getMessage().getContent().startsWith("?") || event.getMessage().getContent().startsWith("？") || event.getMessage().getContent().startsWith("¿")){
            if (!(event.getAuthor().isBot())) {
                Command.onCommand(event.getMessage());
            }
        }
    }
}
