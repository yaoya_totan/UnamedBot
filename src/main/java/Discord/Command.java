package Discord;

import sx.blah.discord.handle.obj.*;
import sx.blah.discord.util.audio.AudioPlayer;

import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.File;
import java.io.IOException;

public class Command {
    public static void onCommand(IMessage message) {
        IUser sender = message.getAuthor();
        IGuild guild = message.getGuild();
        IVoiceChannel userVC = sender.getVoiceStateForGuild(guild).getChannel();
        if (!(userVC == null)) {
            if(!(sender.getVoiceStateForGuild(guild).getChannel().isConnected())){
                userVC.join();
                AudioPlayer audioP = AudioPlayer.getAudioPlayerForGuild(guild);
                File[] songDir = new File("music").listFiles(file -> file.getName().contains("harry"));
                try {
                    audioP.queue(songDir[0]);
                } catch (IOException | UnsupportedAudioFileException e) {
                    e.printStackTrace();
                }
                try {
                    Thread.sleep(20000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                userVC.leave();
            }
        }
    }
}