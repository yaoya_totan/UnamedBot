package Discord;

import sx.blah.discord.api.IDiscordClient;
import sx.blah.discord.api.ClientBuilder;
import sx.blah.discord.api.events.EventDispatcher;
import sx.blah.discord.util.DiscordException;

public class Main {

    public static final IDiscordClient bot = createClient("Token",true);

    public static void main(String args[]){
        System.out.println(bot.getApplicationClientID());
        EventDispatcher dis = bot.getDispatcher();
        dis.registerListener(new Listener());
    }
    public static IDiscordClient createClient (String token, boolean login){
        ClientBuilder cliantBulider = new ClientBuilder();
        cliantBulider.withToken(token);
        try{
            if(login) {
                return cliantBulider.login();
            }
            else {
                return cliantBulider.build();
            }
        }
        catch (DiscordException ex){
            ex.printStackTrace();
            return null;
        }
    }
}
